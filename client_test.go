package client

import (
    "testing"
)

//integration test: requires the server to be running and requestable
func TestClientServer(t *testing.T) {

    idToAes := make(map[string]string)

	cases := []struct{ id, payload string} {
		{"key    1", "payload 1"},
		{"key    2", "payload     2"},
		{"key    3", "payload        3"},
		{"key    1", "payload replica"}, //override value of Id (for one client)
	}
    
    ec := EncrClient{"clientName"}

	for _, c := range cases {
	    //encryption phase, and storing Aes in client map
        aes, err := ec.Store([]byte(c.id), []byte(c.payload))
        if err != nil {
			t.Errorf("Store err: %s", err)
        }
        idToAes[c.id] = string(aes)
        
        //decryption phase, retrieving payload from server map
        result, err := ec.Retrieve([]byte(c.id), aes)
        if err != nil {
			t.Errorf("Retrive err: %s", err)
        }
        
        //comparing the decrypted results with the original payload
        t.Logf("%s: %s\n", c.id, string(result))
        if string(result) != c.payload {
            t.Errorf("%s != %s", string(result), c.payload)
        }
    }
}

