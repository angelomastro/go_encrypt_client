package client

import (
    "bytes"
    "crypto/aes"
    "crypto/cipher"
    crand "crypto/rand"
    "encoding/base32"
    "encoding/json"
    "errors"
    "io"
    "io/ioutil"
    "log"
    mrand "math/rand"
    "net/http"
    "strings"
)


//helper functions for the Client interface implementation of EncrClient
// request, response schemas (shared with the server)
type ReadRequest struct {
    Id     []byte 
}

type ReadResponse struct {
    Payload []byte
    Err     error
}

type WriteRequest struct {
    Id      []byte
    Payload []byte 
}

type WriteResponse struct {
    Err     error
}


//helper functions for encryption and decryption, random key gen

func randKeyGenerator(n int) []byte {
    const letterBytes =         
            "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
    b := make([]byte, n)
    for i := range b {
        b[i] = letterBytes[mrand.Intn(len(letterBytes))]
    }
    return b
}


func encrypt(key, text []byte) ([]byte, error) {
    block, err := aes.NewCipher(key)
    if err != nil {
        return nil, err
    }
    b := base32.StdEncoding.EncodeToString(text)
    ciphertext := make([]byte, aes.BlockSize+len(b))
    iv := ciphertext[:aes.BlockSize]
    if _, err := io.ReadFull(crand.Reader, iv); err != nil {
        return nil, err
    }
    cfb := cipher.NewCFBEncrypter(block, iv)
    cfb.XORKeyStream(ciphertext[aes.BlockSize:], []byte(b))
    return ciphertext, nil
}

func decrypt(key, text []byte) ([]byte, error) {
    block, err := aes.NewCipher(key)
    if err != nil {
        return nil, err
    }
    if len(text) < aes.BlockSize {
        log.Printf("len(text): %d vs aes.BlockSize: %d\n", 
                    len(text), aes.BlockSize)
        return nil, errors.New("(cipher)text too short")
    }
    iv := text[:aes.BlockSize]
    text = text[aes.BlockSize:]
    cfb := cipher.NewCFBDecrypter(block, iv)
    cfb.XORKeyStream(text, text)
    data, err := base32.StdEncoding.DecodeString(string(text))
    if err != nil {
        return nil, err
    }
    return data, nil
}

func generateId(clientId, keyId string) string {
    var buffer bytes.Buffer
    buffer.WriteString(clientId)
    buffer.WriteString("_")
    buffer.WriteString(keyId)
    return buffer.String()
}


type EncrClient struct {
    Id      string
    //implements "Client" interface
}

func (ec *EncrClient) Store(id, payload []byte) (aesKey []byte, err error) {
    //generate Aes key
    aes := randKeyGenerator(32)
    
    //encrypt the payload
    ciphertext, err := encrypt(aes, payload)
    if err != nil {
        log.Printf("failed encription of payload\n")
        return []byte{}, err
    }

    //send key/encrypted-payload to the server
    oId := generateId(ec.Id, string(id))
    objWrite := WriteRequest{[]byte(oId), ciphertext}
    jsonStream, _ := json.Marshal(objWrite)
    writeResp, err := http.Post("http://localhost:8080/write/", 
                    "text/json", bytes.NewBuffer(jsonStream)) 
    log.Printf("write: %s\n", bytes.NewBuffer(jsonStream))
    if err != nil { 
       log.Printf("server did not respond to write request\n")
       return []byte{}, err
    }       
    defer writeResp.Body.Close()
    body, _ := ioutil.ReadAll(writeResp.Body)
    
    //unpack the response from string to a json object 
    var r WriteResponse
    dec := json.NewDecoder(strings.NewReader(string(body)))
    if err := dec.Decode(&r); err != nil {
       log.Printf("failed to unpack json response\n")
       return []byte{}, err
    }
    
    if r.Err != nil {
        log.Print(r.Err)
        return []byte{}, r.Err
    } else {
        //return the Aes key, once all operation succeeded 
        return aes, err
    }
}

func (ec *EncrClient) Retrieve(id, aesKey []byte) (payload []byte, err error){
    //get the encrypted payload from the server
    oId := generateId(ec.Id, string(id))
    objRead := ReadRequest{[]byte(oId)}
    jsonStream, _ := json.Marshal(objRead)
    strStream := string(jsonStream)
    strReq := []string{"http://localhost:8080/read", strStream}
    readResp, err := http.Get(strings.Join(strReq, "/")) 
    log.Printf("read: %s\n", strings.Join(strReq, "/"))
    if err != nil{ 
        log.Printf("No response\n")
        return []byte{}, err
    }
    defer readResp.Body.Close()
    body, _ := ioutil.ReadAll(readResp.Body)

    //unpack the response from string to a json object 
    var r ReadResponse
    dec := json.NewDecoder(strings.NewReader(string(body)))
    if err := dec.Decode(&r); err != nil {
       log.Printf("failed to unpack json response\n")
       return []byte{}, err
    }

    //decrypt it 
    decrPayload, err := decrypt(aesKey, r.Payload)
    if err != nil {
        log.Printf("failed to decrypt payload\n")
        return []byte{}, err
    }    
    
    //return it
    return decrPayload, err    
}


//http://localhost:8080/write/%7B%22Id%22:%22a2V5ICAgIDI=%22,%22Payload%22:%22u6qhMpE9cZLqy5AQHbxzszARlPtIZiijQR6sAYXO5q8TrFrbBDs1Sw==%22%7D


//https://app-encr-server.herokuapp.com/write/%7B%22Id%22:%22a2V5ICAgIDI=%22,%22Payload%22:%22u6qhMpE9cZLqy5AQHbxzszARlPtIZiijQR6sAYXO5q8TrFrbBDs1Sw==%22%7D


