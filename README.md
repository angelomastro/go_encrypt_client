# Encrpytion Client  #

This is a client app sending HTTP requests to the server in go_encrypt_server.

Make sure the server is running before using this.

### Download it ###
$ git clone https://angelomastro@bitbucket.org/angelomastro/go_encrypt_client.git bitbucket.org/client

### Install it in your go-workspace ###
$ go get bitbucket.org/client

### Build the library ###
$ go build bitbucket.org/client

### Test it ###
$ go test bitbucket.org/client